import { shallow } from 'enzyme';
import React from 'react';
import Card from './Card';

it('expect to render Cart component', () => {
  expect(shallow(<Card />)).toMatchSnapshot();
});