import { shallow } from 'enzyme';
import React from 'react';
import CardList from './CardList';

it('expect to render CartList component', () => {
  const mockRobots = [
    {
      id: 1,
      name: 'John',
      email: 'john@gmail.com'
    }
  ]
  expect(shallow(<CardList robots={mockRobots} />)).toMatchSnapshot();
})